import datetime

def generate_dates(year):
    table = {}
    start = datetime.datetime(year, 1, 1)
    tt = start
    i = 0
    while True:
        key = f"{tt.year}-{tt.month:02d}-{tt.day:02d} {tt.hour:02d}:{tt.minute:02d}"
        table[key] = i
        tt = tt + datetime.timedelta(hours = 1)
        if tt.year > year:
            break
        i += 1
    return table

def generate_inversion(year):
    start = datetime.datetime(year, 1, 1)
    tt = start
    i = 0
    while True:
        yield i, tt
        tt = tt + datetime.timedelta(hours = 1)
        if tt.year > year:
            break
        i += 1

if __name__ == "__main__":
    table = generate_dates(2020)
    print(len(table))