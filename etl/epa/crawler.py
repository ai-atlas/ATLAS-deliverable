import pandas as pd 
import datetime
import requests
import zipfile
import os
import pyarrow as pa
import pyarrow.parquet as pq
import numpy as np
import yaml


target_dir = "/data/atlas_data/tmp"
pq_dir =  "/data/atlas_data/usa_epa"
# npz_dir = "/home/atlas/data/atlas_data/usa_np_2021-11-14"
# npz_dir = "/home/atlas/data/atlas_data/usa_np_2022_02_07"
# sites 
def load_sites():
    filename = "aqs_sites.csv"
    df = pd.read_csv(filename)
    aa = df["Site Number"].unique()
    print(aa.shape)

filename = "map.yaml"

with open(filename) as file:
    text = file.read()
    data = yaml.safe_load(text)

url = data['Url_template']

def download(url, CODE, YEAR):
    st = datetime.datetime.now()
    rurl = url.replace('CODE', str(CODE))
    rurl = rurl.replace('YEAR', str(YEAR))
    file_name = rurl.split('/')[-1]
    print(rurl)
    ufile_name = file_name.replace('.zip', '.csv')
    csv_filename = os.path.join(target_dir, ufile_name)
    # if not os.path.exists(csv_filename):
    req = requests.get(rurl)
    file = open(file_name, 'wb')
    for chunk in req.iter_content(100000):
        file.write(chunk)
    file.close()

    print(file_name)
    with zipfile.ZipFile(file_name,"r") as zip_ref:
        zip_ref.extractall(target_dir)
    # print(target_dir)
    os.remove(file_name)
    # df = pd.read_csv(csv_filename, low_memory=False)
    et = datetime.datetime.now()
    filter_by_station(CODE, YEAR)
    print(et-st, csv_filename)


CODEs = [
    '44201', # Ozone
    '42401', # SO2
    '42101', # CO
    '42602', # NO2

    'VOCS', 
    'HAPS', 
    'NONOxNOy',
    'LEAD',

    '88101', # PM2.5 FRM/FEM Mass
    '81102', # PM10 Mass
    'SPEC',
    '88502',
    'PM10SPEC',
    # 'TEMP', # Temperature
    # 'PRESS',
    # 'WIND',
    # 'RH_DP'
]

params = [x.strip() for x in """
    State Code
    County Code
    Site Num
    Parameter Code
    POC
    Latitude
    Longitude
    Datum
    Parameter Name
    Units of Measure
    MDL
    Uncertainty
    Qualifier
    Method Type
    Method Code
    Method Name
    State Name
    County Name
""".split("\n") if len(x.strip())>0]


def filter_by_station(CODE, year):
    filename = f"/data/atlas_data/tmp/hourly_{CODE}_{year}.csv"
    df = pd.read_csv(filename)
    print(df.shape, filename)
    PCODEs = df["Parameter Code"].unique()
    for PCODE in PCODEs:
        dfn = df[df["Parameter Code"]==PCODE]
        parameter_name = dfn["Parameter Name"].unique()[0]
        states = dfn['State Code'].unique()
        for state in states:
            dff = dfn[dfn['State Code'] == state]
            counties = dff['County Code'].unique()
            for county in counties:
                dfff = dff[dff['County Code'] == county]
                sites = dfff['Site Num'].unique()
                for site_num in sites:
                    df_measurements = dfff[dfff['Site Num'] == site_num].copy()
                    site_id = f"{state}-{county}-{site_num}"
                    folder = os.path.join(pq_dir, site_id)
                    os.makedirs(folder, exist_ok=True)
                    df_measurements = df_measurements.astype({"Qualifier": str})
                    df_measurements['datetime'] = df_measurements.apply(lambda x: x["Date GMT"]+' '+x["Time GMT"][:2]+":00:00", axis=1)
                    df_measurements['datetime'] =  pd.to_datetime(df_measurements['datetime'])
                    method_codes = df_measurements['Method Code'].unique()
                    for m_code in method_codes:
                        pocs = df_measurements['POC'].unique()
                        for POC in pocs:
                            filename = f'{PCODE}-{POC}-{m_code}-{year}.parquet.gzip'
                            df_m = df_measurements[(df_measurements['Method Code'] == m_code) & (df_measurements['POC']==POC)]
                            df_m = df_m.sort_values(["datetime"], ascending=True)
                            to_filename = os.path.join(folder, filename)
                            df_m.to_parquet(to_filename, compression='gzip')
                            prefix = ''
                            if df_m.shape[0]>8760:
                                prefix = '========='
                            if df_m.shape[0] == 0:
                                continue
                            print(PCODE, parameter_name, year, state, county, site_num, m_code, df_m.shape, to_filename, prefix)
                            # method_name = df_m['Method Name'].unique()[0]
                            # method_type = df_m['Method Type'].unique()[0]
                            # with open('parameters.csv', 'a') as file:
                            #     print(f"{year},{PCODE},{parameter_name.replace(',', ' ')},{state},{county},{site_num},{m_code},{method_name.replace(',', ' ')},{method_type.replace(',', ' ')},{POC},{df_m.shape[0]}", file=file)


for YEAR in [2021, 2020, 2019, 2018, 2017, 2016, 2015]:
    for CODE in CODEs:
        # print(CODE, YEAR)
        download(url, CODE, YEAR)

# filter_by_station("VOCS", 2021)

# df = pd.read_csv('parameters.csv')
# print(df.shape)
# print(df.head())
# with open('parameters.csv') as file:
#     all = file.read()
#     lines = all.split('\n')
#     for line in lines:
#         print(len(line.split(',')))




# # # # filename = "/home/atlas/data/atlas_data/usa_epa/9-9-27/81102-2020.parquet.gzip"
# # # # filename = "/home/atlas/data/atlas_data/usa_epa/1-73-23/81102-2020.parquet.gzip"
# # filename = "/home/atlas/data/atlas_data/usa_epa/37-183-14/81102-2020.parquet.gzip"
# filename = "/home/atlas/data/atlas_data/usa_epa/6-19-11/81102-3-122-2020.parquet.gzip"

# df = pd.read_parquet(filename)
# print(df.head())

# print(df.shape)

# dtime = df['datetime'].copy()
# dtime_next = df['datetime'].shift(periods=1)



# for p in params:
#     print(p, df[p].unique())

# print(dtime[:10])
# print(dtime_next[:10])
# dd = dtime[1:] - dtime_next[1:]
# print(dd.mean(), dd.std())