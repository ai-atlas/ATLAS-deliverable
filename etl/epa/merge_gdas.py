from importlib.metadata import metadata
from ntpath import join
import pandas as pd
import numpy as np
import os
import zipfile
import matplotlib.pyplot as plt


pq_dir =  "/data/atlas_data/usa_epa"
pq_dir_merged =  "/data/atlas_data/usa_epa_merged"

gdas_root = "/home/atlas/data/atlas_data/usa_gdas"
directory_to_extract_to = os.path.join(gdas_root, 'all')


def extract_all():
    for i in range(1, 6):
        folder = os.path.join(gdas_root, f"gdas1_4stations_US_part{i}")
        print(folder)
        for ff in os.listdir(folder):
            filepath = os.path.join(folder, ff)
            print(filepath)
            with zipfile.ZipFile(filepath, 'r') as zip_ref:
                zip_ref.extractall(directory_to_extract_to)
            # break

# extract_all()

def check_gdas():
    for ff in os.listdir(directory_to_extract_to):
        filepath = os.path.join(directory_to_extract_to, ff)
        df = pd.read_csv(filepath)
        print(df.columns)

# check_gdas()

def merge_gdas():
    epa_folder = "/home/atlas/data/atlas_data/usa_epa"
    for ff in os.listdir(directory_to_extract_to):
        filepath = os.path.join(directory_to_extract_to, ff)
        df = pd.read_csv(filepath)
        site_id = ff.replace("_gdas1.csv", "")
        print(site_id)
        # print(df.head())
        parts = [int(x) for x in site_id.split('_')]
        site_id = f"{parts[0]}-{parts[1]}-{parts[2]}"
        site_epa_folder = os.path.join(epa_folder, site_id)
        if os.path.exists(site_epa_folder):
            for fa in os.listdir(site_epa_folder):
                print(fa)
                # dff = pd.read_parquet(os.path.join(site_epa_folder, fa))
                # print(dff.head())
            print('-------------------')
        else:
            print(f'\tmissing {site_id}')
# merge_gdas()

def test():
    epa_folder = "/home/atlas/data/atlas_data/usa_epa"
    site_id = "21-61-501"
    site_folder = os.path.join(epa_folder, site_id)

    files = []
    for ff in os.listdir(site_folder):
        print(ff)
        files.append(ff)
    measurements = {}
    measurements_df = {}
    for f in files:
        name = f.replace('.parquet.gzip', '')
        parts = name.split('-')
        measurement_id = f'{parts[0]}-{parts[1]}-{parts[2]}'
        year = parts[3]
        if measurement_id in measurements:
            measurements[measurement_id].append(name)
        else:
            measurements[measurement_id] = [name]
    
    for measurement_id in measurements:
        dffs = []
        for ff in measurements[measurement_id]:
            dff = pd.read_parquet(os.path.join(site_folder, f'{ff}.parquet.gzip'))
            dffs.append(dff)
        dff = pd.concat(dffs)
        columns = {}
        for cc in dff.columns:
            if cc != 'datetime':
                columns[cc] = f'{cc}_{measurement_id}'
        dff = dff.rename(columns=columns)
        measurements_df[measurement_id] = dff

    result = None
    for measurement_id in measurements_df:
        dff = measurements_df[measurement_id]
        dff.set_index('datetime')
        dff_name = ff.replace('.parquet.gzip', '')
        parts = dff_name.split('-')
        dff_name = f"{parts[0]}-{parts[1]}-{parts[2]}"

        if result is None:
            result = dff
        else:
            result = result.merge(dff, on='datetime', how='outer', suffixes=('',''))#, left_on="datetime", right_on="datetime")
        print(ff, dff.shape)
    print(result.shape)
    for cc in result.columns:
        print(cc)

# test()
def analize_pq_file(measurement, filename):
    dff = pd.read_parquet(filename)
    print(f'\t{measurement}\t{dff.shape}')
    for column in dff.columns:
        values = dff[column].unique()
        if len(values) == 1:
            print(f'\t\t{column}:{values[0]}')
        else:
            print(f'\t\t{column}[{len(values)}]')

def merge_one_site(site):
    columns = [
        "State Code",
        "County Code",
        "State Name",
        "County Name",
        "Site Num",
        "Parameter Code",
        "POC",
        "Latitude",
        "Longitude",
        "Datum",
        "Parameter Name",
        # "Date Local",
        # "Time Local",
        # "Date GMT",
        # "Time GMT",
        # "Sample Measurement",
        "Units of Measure",
        "MDL",
        "Uncertainty",
        "Qualifier",
        "Method Type",
        "Method Code",
        "Method Name",

        # "Date of Last Change",
        # "datetime"
    ]
    metadata = {}
    metadata['year'] = []
    metadata['site_id'] = []
    metadata['measurement_id'] = []
    metadata['shape'] = []
    for c in columns:
        metadata[c] = []

    parts = [int(x) for x in site.split('-')]
    site_id = f'{parts[0]:02d}-{parts[1]:03d}-{parts[2]:04d}'
    # if site_id != '06-015-0007':
    #     return
    print(site_id)
    site_folder = os.path.join(pq_dir, site)
    # group_by_year
    measurements = {}
    for f in os.listdir(site_folder):
        name = f.replace('.parquet.gzip', '')
        parts = name.split('-')
        measurement_id = f'{parts[0]}-{parts[1]}-{parts[2]}'
        year = parts[3]
        if year in measurements:
            measurements[year].append(name)
        else:
            measurements[year] = [name]

        # columns = {}
        # for cc in dff.columns:
        #     values = dff[cc].unique()
        #     if cc != 'datetime' and len(values)==1:
        #         columns[cc] = f'{cc}_{measurement_id}'
        # dff = dff.rename(columns=columns)

    measurements_df = []
    for year in measurements:
        dffs = None
        for ff in measurements[year]:
            parts = ff.split('-')
            measurement_id = f'{parts[0]}-{parts[1]}-{parts[2]}'
            df_filename = os.path.join(site_folder, f'{ff}.parquet.gzip')
            dff = pd.read_parquet(df_filename)
            if dff.shape[0]>0:
                df_new = pd.DataFrame()
                df_new['datetime'] = dff['datetime']
                df_new[measurement_id] = dff['Sample Measurement']
                print(year, measurement_id)
                metadata['site_id'].append(site_id)
                metadata['year'].append(year)
                metadata['measurement_id'].append(measurement_id)
                metadata['shape'].append(dff['Sample Measurement'].shape[0])
                for c in columns:
                    values = dff[c].unique()
                    if len(values) == 1:
                        metadata[c].append(values[0])
                    else:
                        metadata[c].append(values)

                if dffs is None:
                    dffs = df_new
                else:
                    dffs = dffs.merge(df_new, on='datetime', how='outer', suffixes=('',''))
        measurements_df.append(dffs)

    result = pd.concat(measurements_df)
    result.sort_values(by=['datetime'])
    to_filename = os.path.join(pq_dir_merged, f'{site_id}.parquet.gzip')
    result.to_parquet(to_filename, compression='gzip')

    # fig, a =  plt.subplots(1,1, figsize=(10, 5))
    # t = result['datetime']
    # for c in result.columns:
    #     if c != 'datetime':
    #         a.scatter(t, result[c], label=c)

    # a.grid()
    # a.set_title(f'{site_id}')
    # a.set_xlabel('datetime')
    # # a.set_ylabel(f'{site_id}')
    # a.legend(loc='upper left')
    # plt.savefig(f'site_{site_id}.jpeg')
    # print(result.shape)

    # print(f'\t{result.shape}')
    # for column in result.columns:
    #     values = result[column].unique()
    #     if len(values) == 1:
    #         print(f'\t\t{column}:{values[0]}')
    #     elif len(values) == 2:
    #         print(f'\t\t{column}:{values[0]},{values[1]}')
    #     else:
    #         print(f'\t\t{column}[{len(values)}]')
    print('--------------------')
    return metadata

def merge_epa():
    dfs = []
    for site in os.listdir(pq_dir):
        metadata = merge_one_site(site)
        df_m = pd.DataFrame(metadata)
        dfs.append(df_m)
        if len(dfs)>1:
            df = pd.concat(dfs)
            df.to_csv('metadata.csv')


merge_epa()



        # df_filename = os.path.join(site_folder, f'{name}.parquet.gzip')
        # dff = pd.read_parquet(df_filename)
        # for c in columns:
        #     values = dff[c].unique()
        #     if len(values)>1:
        #         print(f'ERROR {c}: {len(values)}')
        #     if len(values)==2:
        #         for v in values:
        #             print(f'\t{v}\t{dff[dff[c]==v].shape}')
        # # print(dff["Qualifier"].unique())