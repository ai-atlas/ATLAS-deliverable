# ATLAS - deliverable

## 13  D4.1	Software library of primitives for fuzzy spatial modeling	
          An open access software 
          library providing software components with standardized interfaces for 
          fuzzy spatio temporal processes modeling primitives related to elements, structure, 
          operations, state, and state transitions was implemented and tested.	
          Open data
          https://gitlab.com/obradovic.djordje/linearfuzzyspace

## 14  D4.2	Software tool for managing complex planar fuzzy spatial models	
          A software tool, 
          encompassing execution code, source code, technical and user documentation, as well 
          as installation scripts, for fuzzy processes models managing was implemented and tested.	
          Software

## 15	D4.3	Software library of primitives for advanced spatio temporal process modeling	
          An open 
          access library containing source code, and documentation for all fuzzy spatio temporal 
          processes modeling primitives (elements, structure, operations, state, and state transitions) was implemented.

## 21	D5.3	Virtual experiment	
          The key concept underlying the Platform, the model and implementation 
          of this concept was reported both as a technical report and conference/journal papers.	New process
          venecija

## 23	D5.5	Presentation layer	
          An environment hosting collection of virtual experiments built by 
          library of web UI components for tables, diagrams, graphs and GIS representations was provided.	Models


