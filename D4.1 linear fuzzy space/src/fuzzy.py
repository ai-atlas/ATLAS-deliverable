import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

class LinearFuzzyPoint():

    def __init__(self, name, point):
        self.name = name
        self.point = point

    def membership_value(self, x):
        y = (1-np.abs(x-self.point[0])/self.point[1])
        y = np.clip(y, 0, 1)
        return y


AQI_BREAKPOINTS = {
    '44201 Ozone' : [
        [0, 0.125, 0.164, 0.204, 0.404, 0.5, 0.6], 
        [0, 101,   150,    200,  300, 400, 500],
        [0.02, 0.02, 0.02, 0.02, 0.02,0.02, 0.02],
        [15, 15, 20, 30, 30, 30, 40]],
    '42101 CO' : [
          [0, 4.4, 9.4, 12.4, 15.4, 30.4, 40.4, 50.4],
          [0, 50,  100,  150,  200,  300,  400,  500],
          [0.1, 0.2, 0.4, 0.4, 0.4, 1, 2, 3],
          [10, 10,   10,  20,  20, 30, 30, 30]],
    '42401 SO2' : [
        [0, 35,  75, 185],
        [0, 50, 100, 150],
        [10, 10, 10, 10],
        [10, 10, 10, 10]],
    '42602 NO2' : [
        [0, 53,  100, 360],
        [0, 50, 100, 150],
        [10, 10, 10, 10],
        [10, 10, 10, 10]]
}

PSI_BREAKPOINTS = { # 
    '81102' : [ # mikro g per m3
        [0,     50,  150,  350,  420,   500,  600],
        [0,     50,  100,  200,  300,   400,  500]
    ],
    '42401 SO2' : [ # ppm
        [0, 0.03,  0.14, 0.3, 0.6, 0.8, 1],
        [0,   50,  100,  200,  300,   400,  500],
    ],
    '42101 CO' : [ # ppm
          [0,   4.5,    9,   15,   30,    40, 50],
          [0,    50,  100,  200,  300,   400,  500]
    ],
    '42602 NO2' : [ # ppm
        [0, 0.6,  1.2, 1.6, 2],
        [0, 200,  300, 400, 500]
    ],
    '44201 Ozone' : [ # ppm
        [0, 0.06, 0.12,  0.2,  0.4, 0.5, 0.6], 
        [0,   50,  100,  200,  300, 400, 500]
    ],
}

units = {
    "81102": "Micrograms/cubic meter (25 C)",
    "44201 Ozone": "ppm",
    "42101 CO": "ppm",
    "42401 SO2": "ppb",
    "42602 NO2": "ppb"
}

scales = {
    "81102": 1, # PM10  Micrograms/cubic meter (25 C)
    "44201 Ozone": 1, # Parts per million
    "42101 CO": 1, # Parts per million
    "42401 SO2": 0.001, # Parts per billion
    "42602 NO2": 0.001 # Parts per billion
}

LABELS = {
    '81102': 'PM10',
    '42401 SO2': 'SO2',
    '42101 CO': 'CO',
    '42602 NO2': 'NO2',
    '44201 Ozone': 'O3'
}

def calculate_faqi(df, target, point):
    lfp = LinearFuzzyPoint(point)
    df[f'{target} FAQI'] = lfp.membership_value(df[f'{target} AQI'])

def calculate_faqi_2(df, target, points):
    y = df[f'{target} AQI']
    a = np.interp(y, points[0], points[1])
    df[f'{target} FAQI'] = y

def translate_to_FAQI(df, name):
    t = df.dt
    x = df[name]
    y = np.interp(x, AQI_BREAKPOINTS[name][0], AQI_BREAKPOINTS[name][1])
    yr = np.interp(x, AQI_BREAKPOINTS[name][0], AQI_BREAKPOINTS[name][3])
    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.plot(t, y+yr, linestyle=':', color='k', label=name)
    a.plot(t, y, color='k', label=name)
    a.plot(t, y-yr, linestyle=':', color='k', label=name)

    a.grid()
    a.set_title(f'{name}')
    a.set_xlabel('time')
    a.set_ylabel(name)
    a.legend(loc='upper left')
    plt.savefig(f'imgs/{name}_fuzzy_time.jpeg')

def show_plot(name):
    xb = np.array(AQI_BREAKPOINTS[name][0])
    x = np.linspace(xb.min(), xb.max(), 200)

    y = np.interp(x, AQI_BREAKPOINTS[name][0], AQI_BREAKPOINTS[name][1])
    yr = np.interp(x, AQI_BREAKPOINTS[name][0], AQI_BREAKPOINTS[name][3])


    fig, a =  plt.subplots(1,1, figsize=(10, 5))
    a.plot(x, y)
    plt.plot(x, y+yr/2, linestyle=':', color='k', lw=2)
    plt.plot(x, y-yr/2, linestyle=':', color='k', lw=2)
    plt.fill_between(x, y+yr/2, y-yr/2, facecolor='yellow', alpha=0.2)

    a.grid()
    a.set_title(f'O3 PSI')
    a.set_ylabel('PSI')
    a.set_xlabel('concetration [ppm]')


    for i in range(len(AQI_BREAKPOINTS[name][0])):
        x0 = AQI_BREAKPOINTS[name][0][i]
        y0 = AQI_BREAKPOINTS[name][1][i]
        xr = AQI_BREAKPOINTS[name][2][i]
        yr = AQI_BREAKPOINTS[name][3][i]
        p0 = mpl.patches.Ellipse((x0, y0), xr, yr, color='r', fill=False)
        a.add_patch(p0)

    plt.savefig(f'imgs/{name}_PSI_func.jpeg')

