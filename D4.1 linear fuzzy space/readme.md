# LinearFuzzySpace

Software library of primitives for fuzzy spatial modelling

![fuzzy point](imgs/fuzzy_point.png)

## Abstract

LinearFuzzySpace is Python package/library implementation of basic planar imprecise geometric objects (fuzzy point, fuzzy line, fuzzy triangle and fuzzy circle). Also, basic measurement functions (distance between fuzzy point and fuzzy line, fuzzy point and fuzzy triangle, two fuzzy lines and two fuzzy triangles) as well as spatial operation (linear combination of two fuzzy points) and main spatial relations (coincidence, between and collinear)are implemented. 

Results obtained with our model can be used in various applications such as image analysis (imprecise feature extraction), GIS (imprecise spatial object modeling), robotics (environment models). Imprecise point objects are modeled as a union of linear combinations of fuzzy points in linear fuzzy space. However, it is proved that fuzzy line could be represented only by two and fuzzy triangle with three fuzzy points.

![linear combination](imgs/linear_combination.png)


## Authors

Đorđe Obradović, Zora Konjović, Endre Pap

djobradovic@singidunum.ac.rs, zkonjovic@singidunum.ac.rs, epap@singidunum.ac.rs

## Papers

[Fuzzy Geometry in Linear Fuzzy Space](https://link.springer.com/chapter/10.1007/978-3-642-33959-2_8)

[Linear fuzzy space based road lane model and detection](https://www.sciencedirect.com/science/article/abs/pii/S0950705112000032)

[Linear fuzzy space with applications](https://ieeexplore.ieee.org/document/8246259)

[Linear fuzzy space polygon based image segmentation and feature extraction](https://ieeexplore.ieee.org/document/6339581)

[The maximal distance between imprecise point objects](https://www.sciencedirect.com/science/article/abs/pii/S0165011410004847)

[Extending PostGIS by imprecise point objects](https://ieeexplore.ieee.org/abstract/document/5647464)


[Linear Fuzzy Space Based Scoliosis Screening](https://d1wqtxts1xzle7.cloudfront.net/47179002/Linear_Fuzzy_Space_Based_Scoliosis_Scree20160712-16221-lb0tuk.pdf?1468309190=&response-content-disposition=inline%3B+filename%3DLinear_Fuzzy_Space_Based_Scoliosis_Scree.pdf&Expires=1614938969&Signature=NSlbvsB6agx2Bk0ClOy~0YQ4Z2ZRdtCsZ9UCJxUstodd0JaxzyFS0qN9z58jBAXQ~JWaLj-j1XpIsKp6IZADY1pDA0PdZx2ZCmEISHyw-fJ6P5RYL1HAXL-irIpNe7G~dDk961JPosT7E~oNDbYU~Ynk17ECVe7LLLvjP2wnlavPIkIi2rh3rCZXyvxYu1dR6hXJmPkOXAcUHZ5rTRYFVW61AGjN0CawzXqEBa-Vlt~dvUL9d5vhUgSYsfoQcG0MkKpmnGJ-6SI0JM5H3Lu16432bYNhO1h6h8mV7VdFFRyEwUGoN9MdHE9EIc-8r4fPZKiuC4PL9eG7EZf98kaDaQ__&Key-Pair-Id=APKAJLOHF5GGSLRBV4ZA)

## Acknowledgments

The authors acknowledge funding provided by the Science Fund of the Republic of Serbia #GRANT No. 6524105, AI – ATLAS.