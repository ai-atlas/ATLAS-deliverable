import pandas as pd
import numpy as np
import os
import yaml
import util

import matplotlib.pyplot as plt
import matplotlib as mpl

from xgboost import XGBRegressor
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold

PREDICTORS = [
    "PRSS",
    "TPP6",
    "RH2M",
    "T02M",
    "TCLD",
    "U10M",
    "V10M",
    "TMPS",
    "PBLH",
    "irradiance",
]

def prepare_predictors(predictor, use_1h_history):
    predictors = []
    for el in PREDICTORS:
        if predictor == 'ALL':
            predictors.append(el)
            if use_1h_history:
                predictors.append(f'{el}_1')
        else:
            if predictor != el:
                predictors.append(el)
                if use_1h_history:
                    predictors.append(f'{el}_1')
    return predictors

def reporting_line(site_id, use_1h_history, cm, predictor, is_classification):
    if is_classification:
        good = cm[0]+cm[1]
        fail = cm[2]+cm[3]
        accuracy = 100*good/(good+fail)
        print(f'{site_id},{use_1h_history},{predictor},{cm[0]},{cm[1]},{cm[2]},{cm[3]},{accuracy:2.5f},{cm[4]:2.5f},{cm[5]:2.5f}')
    else:
        print(f'{site_id},{use_1h_history},{predictor},{cm[0]:2.5f},{cm[1]:2.5f}')


def experiment_generator(site_id, df, target, estimators, is_classification):
    use_1h_history_options = [True, False]
    for use_1h_history in use_1h_history_options:
        predictors = prepare_predictors('ALL', use_1h_history)
        cm = single_experiment(df, predictors, target, estimators, is_classification)
        reporting_line(site_id, use_1h_history, cm, 'ALL', is_classification)
        for predictor in PREDICTORS:
            predictors = prepare_predictors(predictor, use_1h_history)
            cm = single_experiment(df, predictors, target, estimators, is_classification)
            reporting_line(site_id, use_1h_history, cm, predictor, is_classification)

def single_experiment(dd, predictors, target, ESTIMATORS, is_classification):
    # x = dd[predictors]
    # y = dd[target]
    # cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
    # scores = cross_val_score(model, x, y, scoring='roc_auc', cv=cv, n_jobs=-1)
    # print(f'Mean ROC AUC: {np.mean(scores):2.5f}')
    ps = []
    for i in range(5):
        if is_classification:
            model = XGBClassifier(
                        scale_pos_weight=10000, 
                        use_label_encoder=False,
                        n_estimators=ESTIMATORS, 
                        max_depth=4, 
            )
        else:
            model = XGBRegressor(
                        use_label_encoder=False,
                        n_estimators=ESTIMATORS, 
                        max_depth=4, 
            )
        train, test = train_test_split(dd, test_size=0.4)
        x_train = train[predictors]
        y_train = train[target]
        x_test = test[predictors]
        y_test = test[target]
        eval_set = [(x_test, y_test)]
        model.fit(x_train, y_train, eval_metric="error", eval_set=eval_set, verbose=False)
        y_predicted_test = model.predict(x_test)
        if is_classification:
            p = confusion_matrix(y_test, y_predicted_test)
            c1 = np.where(y_test==1)[0].shape[0]
            cc = 100*p[1][1]/c1
            pp = np.array([p[0][0], p[1][1], p[1][0], p[0][1], c1, cc])
        else:
            err = (y_predicted_test-y_test).abs()
            util.regression_error(y_test, y_predicted_test, target)
            pp = np.array([err.mean(), err.std()])
        ps.append(pp)
    ps = np.array(ps)
    
    return ps.mean(axis=0)#.astype(np.int32)
