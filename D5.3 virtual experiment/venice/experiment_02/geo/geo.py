import pandas as pd
import numpy as np

stanice = pd.read_csv("../../data/usa_btex_sites.csv")

print(stanice.columns)

df_stanice = stanice.set_index('id')
print(df_stanice)

site_ids = [
    "11-001-0043", #(49587, 24) (27981, 385)
    "13-089-0002", #(44817, 24) (21468, 385)
    "18-097-0078", #(44147, 24) (16774, 385)
    "22-033-0009", #(49886, 24) (6569, 385)
    "32-003-0540" #(29777, 24) (9665, 385)
]

for site_id in site_ids:
    print(df_stanice[site_id])