import pandas as pd
import numpy as np


df = pd.DataFrame()

df['dt'] = [1, 2, 3, 4, 7, 8, 9, 10, 11, 12]
df['x']  = [1, 2, 3, 4, 7, 8, 9, 10, 11, 12]
df['y']  = [0, 0, 0, 1, 1, 0, 0,  0,  0,  0]

df0 = df[df.y == 0].copy()
df1 = df[df.y == 1].copy()


# print(df1)
# print()

# print(df)

# print()

# for i in range(2):
#     ind0 = df0.sample(1)
#     ind1 = df1.sample(1)
#     df.loc[ind0.index, :] = ind1.values
# print(df)


df['ry'] = df.y.shift(periods=1)

print(df)

# a = np.array([0, 0, 1, 1, 0])

# b = np.where(a==1)

# print(b[0].shape)

dd = pd.DataFrame()
dd['dt'] = df.dt
for el in ['dt', 'x', 'y']:
    dd[el] = df[el]
for pre in range(1, 4):
    dt_pre = df.dt.shift(periods=pre)
    # dd[f'delta_{pre}'] = dd.dt - dt_pre
    delta = dd.dt - dt_pre
    for el in ['dt', 'x', 'y']:
        dd[f'{el}_{pre}'] = df[el].shift(periods=pre)
    dd = dd[delta == pre].copy()
# drez = dd[delta==pre].copy()
drez = dd.copy()
print('==================')
print(drez)