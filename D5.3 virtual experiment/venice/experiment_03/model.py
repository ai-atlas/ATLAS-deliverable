import numpy as np
import pandas as pd

from xgboost import XGBRegressor
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

PREDICTORS = [
    "PRSS",
    "TPP6",
    "RH2M",
    "T02M",
    "TCLD",
    "U10M",
    "V10M",
    "TMPS",
    "PBLH",
    "irradiance",
]

ESTIMATORS = 500

def prepare_predictors():
    predictors = []
    for el in PREDICTORS:
        for i in range(1, 24):
            predictors.append(f'{el}_{i}')
    return predictors

def single_experiment(df, target, site_id):
    predictors = prepare_predictors()
    ss = np.array(predictors).copy().tolist()
    ss.append(target)
    dd = df[ss].copy()
    model = XGBRegressor(
                use_label_encoder=False,
                n_estimators=ESTIMATORS, 
                max_depth=4, 
    )
    train, test = train_test_split(dd, test_size=0.2)
    x_train = train[predictors]
    y_train = train[target]
    x_test = test[predictors]
    y_test = test[target]
    eval_set = [(x_test, y_test)]
    model.fit(x_train, y_train, eval_metric="error", eval_set=eval_set, verbose=False)
    y_predicted_test = model.predict(x_test)
    err = (y_predicted_test-y_test).abs()
    print(site_id, target, err.mean(), err.std())

def single_experiment_nn(df, target, site_id):
    def normalize(arr):
        scale = arr.max()
        # if MODEL == 'nn':
        # scale = arr.max()
        return tf.convert_to_tensor(arr/scale) 
    predictors = prepare_predictors()
    ss = np.array(predictors).copy().tolist()
    ss.append(target)
    dd = df[ss].copy()
    train, test = train_test_split(dd, test_size=0.2)
    x_train = train[predictors]
    y_train = train[target]
    x_test = test[predictors]
    y_test = test[target]

    x_train = normalize(x_train)
    y_train = normalize(y_train)
    x_test  = normalize(x_test)
    y_test  = normalize(y_test)
    input_shape = len(predictors)
    output_shape = 1
    model = keras.Sequential(
        [
            keras.Input(shape=input_shape),
            layers.Dropout(0.1),
            layers.Dense(20, activation="relu"),
            layers.Dropout(0.1),
            layers.Dense(output_shape, activation="sigmoid"),
        ]
    )
    model.compile(loss="mse", optimizer="sgd")
    model.fit(x_train, y_train, batch_size=100, epochs=100, validation_split=0.2, verbose=0)
    y_predicted_test = model.predict(x_test)
    y_predicted_train = model.predict(x_train)
    err = np.absolute(y_predicted_test-y_test.numpy())
    print(site_id, target, err.mean(), err.std())