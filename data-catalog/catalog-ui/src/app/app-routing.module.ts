import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SitesComponent } from './sites/sites.component';
import { GdasComponent } from './gdas/gdas.component';
import { DdropComponent } from './ddrop/ddrop.component';
import { TtableComponent } from './ttable/ttable.component';

const routes: Routes = [
  { path: 'sites', component: SitesComponent },
  { path: 'gdas', component: GdasComponent },
  { path: 'ddrop', component: DdropComponent },
  { path: 'ttable', component: TtableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})



export class AppRoutingModule { }
