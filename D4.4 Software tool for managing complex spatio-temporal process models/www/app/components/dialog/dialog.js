(function () {
    'use strict';

    var app = angular.module('myApp')

    app.controller('DialogCtrl',
        function ($rootScope, $scope, $http, $location, $timeout, $window, $document) {
            var vm = this;
            $rootScope.dialog = this;
            vm.showModal = false;
            vm.yesNo = false;
            vm.okLabel = 'Save';
            vm.cancelLabel = 'Cancel';


            vm.fields = [
                { name: "_id", label: '_id', value: '', type:'text' },
                { name: "layer", label: 'layer', value: '', type:'text' },
                { name: "project", label: 'project', value: '', type:'text' },
                { name: "section", label: 'section', value: '', type:'text' },
                { name: "id", label: 'id', value: '', type:'text' },
                { name: "rb", label: 'rb', value: '', type:'text' },
                { name: "color", label: 'color', value: '', type:'text' }

                // { name: "geo", label: 'Geo', value: '', type:'text' },
            ];

            vm.showYesNo = function (message, onOk, onCancel) {
                vm.yesNo = true;
                vm.showModal = true;
                vm.okLabel = 'Yes';
                vm.cancelLabel = 'No';
                vm.message = message;
                vm.onOk = onOk;
                vm.onCancel = onCancel;
            }

            vm.show = function(onOk, onCancel){
                vm.onOk = onOk;
                vm.onCancel = onCancel;

                vm.yesNo = false;
                vm.showModal = true;
                // console.log(vm.showModal)
                // $scope.$apply();
                // $timeout(function () {
                //     jq('#dialog_0').focus();
                // });                
            }

            vm.ok = function(){
                vm.showModal = false;
                var ret = {};
                for(var i in vm.fields){
                    var el = vm.fields[i];
                    ret[el.name] = el;
                }
                vm.onOk(ret);
            }


            vm.cancel = function(){
                vm.showModal = false;
                vm.onCancel();
            }

        }// controller
    ); // app

})();