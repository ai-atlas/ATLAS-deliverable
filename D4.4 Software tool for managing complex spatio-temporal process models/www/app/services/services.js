var app = angular.module('myApp');
app.factory('WoSrv',
    function ($http, $window, $document, $interval, $rootScope) {
        var vm = this;


        vm.roverActivities = function(){
            var u = "api/rtk/rovers_activities"
            if ($rootScope.farm)
                u += '?farm_id=' + $rootScope.farm._id;
            var req = {
                method: 'GET',
                url: u,
                headers: { 'auth-token': $rootScope.token }
            }
            return $http(req).then(handleSuccess, handleError('Error getting user by username'));
        }

        vm.roverTaskGraph = function (roverTask) {
            var u = "/api/rover_task/graph"
            var obj = {
                'rover_id': roverTask.rov_id,
                'start': moment(roverTask.start).toISOString(),
                'end': moment(roverTask.end).toISOString(),
            }
            var req = {
                method: 'PUT',
                data: obj,
                url: u,
                headers: { 'auth-token': $rootScope.token }
            }
            return $http(req).then(handleSuccess, handleError('roverTaskGraph'));
        }

        vm.farmById = function (id) {
            var req = {
                method: "GET",
                url: "api/farms/by_id/"+id
            }
            return $http(req).then(handleSuccess, handleError('Error getting user by username'));
        }

        vm.crops = [];
        vm.initCrops = function (then) {
            var uu = '/api/crops/all';
            if ($rootScope.farm)
                uu += "?farm_id=" + $rootScope.farm._id;

            var req = {
                method: 'GET',
                url: uu,
                headers: { 'auth-token': $rootScope.token }
            }

            $http(req).then(function (res) {
                var lista = res.data.rez;
                vm.count = lista.length;
                for (var i in lista) {
                    delete lista[i].saved;
                }
                vm.tools = lista;
                if (then)
                    then(vm.tools);
            }, function (res) { });
        }

        vm.tools = [];
        vm.initTools = function (then) {
            var uu = '/api/tools/all';
            if ($rootScope.farm)
                uu += "?farm_id=" + $rootScope.farm._id;

            var req = {
                method: 'GET',
                url: uu,
                headers: { 'auth-token': $rootScope.token }
            }

            $http(req).then(function (res) {
                var lista = res.data.rez;
                vm.count = lista.length;
                for (var i in lista) {
                    delete lista[i].saved;
                }
                vm.tools = lista;
                if (then)
                    then(vm.tools);
            }, function (res) { });
        }

        vm.machinery = [];
        vm.initMachinery = function (then) {
            var uu = '/api/machinery/all';
            if ($rootScope.farm)
                uu += "?farm_id=" + $rootScope.farm._id;

            var req = {
                method: 'GET',
                url: uu,
                headers: { 'auth-token': $rootScope.token }
            }

            $http(req).then(function (res) {
                var lista = res.data.rez;
                vm.count = lista.length;
                for (var i in lista) {
                    delete lista[i].saved;
                }
                vm.machinery = lista;
                if (then)
                    then(vm.machinery);
            }, function (res) { });
        }

        vm.employees = [];
        vm.initEmployees = function (then) {
            var fixElem = function (el) {
                el._desc = el.ime_prezime + ' ';
            }

            var uu = '/api/employees/all';
            if ($rootScope.farm)
                uu += "?farm_id=" + $rootScope.farm._id;

            var req = {
                method: 'GET',
                url: uu,
                headers: { 'auth-token': $rootScope.token }
            }

            $http(req).then(function (res) {
                console.log(res);
                var lista = res.data.rez;
                for (var i in lista) {
                    delete lista[i].saved;
                    fixElem(lista[i]);
                }
                vm.employees = lista;
                if (then)
                    then(vm.employees);
            }, function (res) { });
        }


        vm.materials = [];
        vm.initMaterials = function (then) {
            var fixElem = function (el) {
                el._desc = el.producer + ' ';
                el._desc = el.type + ' ';
                el._desc = el.title + ' ';
                el._desc += el.description;
            }

            var uu = '/api/materials/all';
            if ($rootScope.farm)
                uu += "?farm_id=" + $rootScope.farm._id;

            var req = {
                method: 'GET',
                url: uu,
                headers: { 'auth-token': $rootScope.token }
            }

            $http(req).then(function (res) {
                console.log(res);
                var lista = res.data.rez;
                vm.count = lista.length;
                for (var i in lista) {
                    delete lista[i].saved;
                    fixElem(lista[i]);
                }
                vm.materials = lista;
                if(then)
                    then(vm.materials);
            }, function (res) { });
        }

        vm.operations = [];
        vm.initOperations = function (then) {
            var fixElem = function (el) {
                el._desc = el.naziv + ' ';
            }

            var uu = '/api/operations/all';
            if ($rootScope.farm)
                uu += "?farm_id=" + $rootScope.farm._id;

            var req = {
                method: 'GET',
                url: uu,
                headers: { 'auth-token': $rootScope.token }
            }

            $http(req).then(function (res) {
                console.log(res);
                var lista = res.data.rez;
                vm.count = lista.length;
                for (var i in lista) {
                    delete lista[i].saved;
                    fixElem(lista[i]);
                }
                vm.operations = lista;
                if (then)
                    then(vm.operations);
            }, function (res) { });
        }


        vm.parcels = [];
        vm.wos = [];
        vm.plans = [];
        vm.initParcels = function (fun) {
            vm.cc = 3;
            var u = '/api/parcels/all'
            if ($rootScope.farm)
                u += '?farm_id=' + $rootScope.farm._id;
            var req = {
                method: 'GET',
                url: u,
                headers: { 'auth-token': $rootScope.token }
            }
            $http(req).then(function (res) {
                var lista = res.data.rez;
                vm.count = lista.length;
                var rez = [];
                for (var i in lista) {
                    delete lista[i].saved;
                    var parcela = lista[i];
                    var coordinates = [];
                    if (parcela.geo){
                        for (var i in parcela.geo.coordinates[0]) {
                            var point = ol.proj.transform(parcela.geo.coordinates[0][i], 'EPSG:4326', 'EPSG:32634')
                            coordinates.push(point);
                        }
                        parcela.utm_coordinates = coordinates;
                        rez.push(parcela);
                    }
                }
                vm.parcels = rez;
                vm.cc--;
                if (vm.cc == 0)
                    fun();
            }, function (res) { });

            var obj = {}
            obj.query = {
                'status': 0
            }
            if ($rootScope.farm)
                obj.query.farm_id = $rootScope.farm._id;

            obj.sort = ['_id', -1]
            obj.limit = 100
            obj.skip = 0
            var req = {
                method: 'PUT',
                data: obj,
                url: '/api/wo_list',
                headers: { 'auth-token': $rootScope.token }
            }
            $http(req).then(function (res) {
                var lista = res.data.res;
                vm.count = lista.length;
                for (var i in lista) {
                    delete lista[i].saved;
                    var wo = lista[i];
                    var coordinates = [];
                    for (var i in wo.polygon.coordinates[0]) {
                        var point = ol.proj.transform(wo.polygon.coordinates[0][i], 'EPSG:4326', 'EPSG:32634')
                        coordinates.push(point);
                    }
                    wo.utm_coordinates = coordinates;
                }
                vm.wos = lista;
                vm.cc--;
                if (vm.cc == 0)
                    fun();
            }, function (res) { });

            var u = '/api/plans/all';
            if ($rootScope.farm)
                u += '?farm_id=' + $rootScope.farm._id;
            var req = {
                method: 'GET',
                url: u,
                headers: { 'auth-token': $rootScope.token }
            }
            $http(req).then(function (res) {
                var lista = res.data.rez;
                var rez = [];
                for (var i in lista) {
                    delete lista[i].saved;
                    var plan = lista[i];
                    var coordinates = [];
                    if (plan.geo) {
                        for (var i in plan.geo.coordinates[0]) {
                            var point = ol.proj.transform(plan.geo.coordinates[0][i], 'EPSG:4326', 'EPSG:32634')
                            coordinates.push(point);
                        }
                        plan.utm_coordinates = coordinates;
                        rez.push(plan);
                    }
                }
                vm.plans = rez;
                vm.cc--;
                if (vm.cc == 0)
                    fun();
            }, function (res) { });
        }

        vm.initPlans = function(then){
            var u = '/api/plans/all';
            if ($rootScope.farm)
                u += '?farm_id=' + $rootScope.farm._id;
            var req = {
                method: 'GET',
                url: u,
                headers: { 'auth-token': $rootScope.token }
            }
            $http(req).then(function (res) {
                var lista = res.data.rez;
                var rez = [];
                for (var i in lista) {
                    delete lista[i].saved;
                    var plan = lista[i];
                    var coordinates = [];
                    if (plan.geo) {
                        for (var i in plan.geo.coordinates[0]) {
                            var point = ol.proj.transform(plan.geo.coordinates[0][i], 'EPSG:4326', 'EPSG:32634')
                            coordinates.push(point);
                        }
                        plan.utm_coordinates = coordinates;
                        rez.push(plan);
                    }
                }
                vm.plans = rez;
                if(then)
                    then(vm.plans);
            }, function (res) { });
        }

        vm.saveWo = function(wo_obj, then){
            if ($rootScope.farm)
                wo_obj.farm_id = $rootScope.farm._id;
            var obj = {};
            var tasks = wo_obj.tasks;
            for (var k in wo_obj) {
                if (k != 'tasks')
                    obj[k] = wo_obj[k];
            }
            obj.startDate = moment(wo_obj.startDate, 'DD.MM.YYYY').toISOString();
            var req = {
                method: 'PUT',
                data: obj,
                url: '/api/add_wo',
                headers: { 'auth-token': $rootScope.token }
            }

            $http(req).then(function (res) {
                if(then)
                    then(res);
            }, function (res) { });
        }

        vm.listsAreSame = function (arr1, arr2) {
            if (arr1 == null || arr1 == undefined)
                return false
            if (arr2 == null || arr2 == undefined)
                return false
            if (arr1.length !== arr2.length)
                return false;
            for (var i = arr1.length; i--;) {
                if (arr1[i].wo_id !== arr2[i].wo_id)
                    return false;
            }
            return true;
        }


        function handleSuccess(data) {
            return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }


        return vm;
    }
)
