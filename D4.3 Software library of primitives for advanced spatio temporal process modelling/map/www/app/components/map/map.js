(function () {
    'use strict';

    // var jq = $.noConflict();
    var app = angular.module('myApp')
    app.controller('MapCtrl',
        function ($rootScope, $scope, $http, $location, $timeout, $window, $document) {
            var vm = this;
            $rootScope.mapa = this;
            vm.loading = false;

            vm.query = $window.location.search.substring(1);
            vm.qs = parse_query_string(vm.query);
            vm.project = vm.qs.project;

            vm.geometryTypes = ['Point', 'LineString', 'Polygon', 'Box'];

            vm.layer = null;
            vm.source = null;
            vm.selected = [];
            vm.selectedEl = {};

            vm.layers = {}

            vm.rSource = null; // roveri
            vm.cSource = null; // camera
            vm.selectIt = null;

            vm.screenWidth = $window.innerWidth;
            vm.screenHeight = $window.innerHeight;
            angular.element($window).on('resize', function () {
                vm.screenWidth = $window.innerWidth;
                vm.screenHeight = $window.innerHeight;
                $scope.$apply();
            });

            vm.onLabelChanged = function (el) {
                var req = {
                    method: "PUT",
                    data: el,
                    url: "/api/map-light/update_feature_geo"
                }
                $http(req).then(
                    function (resp) {
                    },
                    function (resp) { }
                );

            }

            vm.onObjectSelected = function (obj) {
                var el = obj.get('obj');
                if (el) {
                    console.log(el);
                    var el1 = jq('#pc_viewer_b');
                    // ----------- DIALOG --------------------
                    $rootScope.dialog.fields = [
                        { name: "_id", label: '_id', value: '', type: 'text' },
                        { name: "layer", label: 'layer', value: '', type: 'text' },
                        { name: "project", label: 'project', value: '', type: 'text' },
                        { name: "section", label: 'section', value: '', type: 'text' },
                        { name: "id", label: 'id', value: '', type: 'text' },
                        { name: "rb", label: 'rb', value: '', type: 'text' },
                        { name: "color", label: 'color', value: '', type: 'text' }
                    ];

                    $rootScope.dialog.fields[0].value = el._id;
                    $rootScope.dialog.fields[1].value = el.layer;
                    $rootScope.dialog.fields[2].value = el.project;
                    $rootScope.dialog.fields[3].value = el.section;
                    $rootScope.dialog.fields[4].value = el.id;
                    $rootScope.dialog.fields[5].value = el.rb;
                    $rootScope.dialog.fields[6].value = el.color;
                    // $rootScope.dialog.fields[6].value = el.geo.coordinates;
                    console.log(el.geo.coordinates);
                    $rootScope.dialog.show(function (obj) { // ---- ok -----
                        el.id = obj.id.value;
                        el.layer = obj.layer.value;
                        el.project = obj.project.value;
                        el.section = obj.section.value;
                        el.rb = obj.rb.value;
                        el.color = obj.color.value;
                        var req = {
                            method: "PUT",
                            data: el,
                            url: "/api/map-light/update_feature_geo"
                        }
                        $http(req).then(
                            function (resp) { },
                            function (resp) { }
                        );


                    }, function () { // ------ cancel ------
                        console.log('Cancel');
                    });


                    if ((el.layer == 'Curbs') || (el.layer == 'Road_markings_pc')) {
                        if ($rootScope.learning) {
                            el.label = $rootScope.learning.label;
                            $rootScope.learning.lista.push(el);
                            vm.onLabelChanged(el);
                        }
                    }
                }
            }

            vm.showPosition = function () {
                var parts = vm.command.split(",");
                if (parts.length > 1) {
                    var lon = parseFloat(parts[0]);
                    var lat = parseFloat(parts[1]);
                    var center = [lon, lat];
                    vm.map.getView().animate({ 'center': ol.proj.transform(center, 'EPSG:4326', 'EPSG:3857'), duration: 1000 })
                    var point = ol.proj.fromLonLat(center);
                    var thing = new ol.geom.Point([point[0], point[1]]);
                    var featurething = new ol.Feature({
                        name: "pozicija",
                        obj: {},
                        geometry: thing
                    });
                    vm.rSource.addFeature(featurething);
                }
            }

            vm.getLayers = function () {
                var lista = [];
                for (var key in vm.layers) {
                    var obj = vm.layers[key];
                    obj.name = key;
                    lista.push(obj);
                }
                return lista;
            }

            vm.heading = 0;
            vm.key = function (zEvent) {  // $document.bind("keydown keypress", function(zEvent) {
                console.log(zEvent.keyCode, "r".charCodeAt(0));
                var code = String.fromCharCode(zEvent.keyCode);

                if (zEvent.keyCode == "r".charCodeAt(0)) {
                    //vm.next();
                    vm.reload();
                } else if (code == 'Q') {
                    vm.heading -= 0.1 * Math.PI / 180;
                    vm.showFrame();
                    // vm.draw_grid();
                } else if (code == 'E') {
                    vm.heading += 0.1 * Math.PI / 180;
                    vm.showFrame();
                    // vm.draw_grid();
                } else if (zEvent.keyCode == 65) { // a
                    vm.fid -= 1;
                    vm.fidChanged(vm.fid + 1);
                    // vm.reload();
                } else if (zEvent.keyCode == 27) { // esc
                    if ($rootScope.modalDijalog != undefined) {
                        $rootScope.modalDijalog.cancel();
                        $rootScope.modalDijalog = undefined;
                        $scope.$apply();
                    } else {
                        if (vm.draw)
                            vm.map.removeInteraction(vm.draw);
                    }
                }
            }

            // $document.bind("keydown", vm.key);
            vm.store_layer_meta = function (el) {
                var obj = {};
                obj.name = el.name;
                obj._id = el._id;
                obj.geometryType = el.geometryType;
                obj.visible = el.visible;
                obj.stroke = el.stroke;
                obj.api = el.api;
                obj.style = new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: obj.stroke.color,
                        width: obj.stroke.width
                    })
                })
                obj.shapes = [];
                var req = {
                    method: "PUT",
                    data: obj,
                    url: "/api/map-light/layers_meta"
                };
                console.log('store layer meta', req);
                $http(req).then(
                    function (resp) {
                        console.log('ok')
                    },
                    function (resp) {
                        console.log('greska')
                    }
                );
            }

            vm.features = {};
            vm.refreshView = function () {
                console.log('refresh view');
                vm.source.clear();
                vm.features = {};
                vm.limit = 500;
                vm.skip = 0;
                vm.viewChanged(vm.box);
            }

            vm.reload = function () {
                vm.loading = true;
                vm.features = {};
                vm.selectedEl = {};
                vm.selected = [];
                if (vm.selectedFeatures != undefined)
                    vm.selectedFeatures.clear();
                vm.refreshView();
            }

            vm.extraFeatures = {}
            vm.total = 0;
            vm.skip = 0;
            vm.limit = 5000;

            vm.addPointFeature = function (el, styles) {
                // el.style = style;
                var epoint = ol.proj.fromLonLat(el.geo.coordinates);
                epoint = [epoint[0], epoint[1]];
                var thing = new ol.geom.Point(epoint);
                var featurething = new ol.Feature({
                    name: "Point",
                    obj: el,
                    geometry: thing,
                });
                featurething.setId(el._id);
                var sstyle = styles['default'];
                if(el.AirQualityStationArea in styles){
                    sstyle = styles[el.AirQualityStationArea];
                }
                featurething.setStyle(sstyle);
                if (el.color) {
                    featurething.setStyle(createPointStyle(el.color, 5, 1));
                }
                vm.source.addFeature(featurething);
                vm.features[el._id] = featurething;
            }

            vm.addLineStringFeature = function (el, style) {
                var coordinates = [];
                for (var i in el.geo.coordinates) {
                    var point = ol.proj.fromLonLat(el.geo.coordinates[i]);
                    coordinates.push([point[0], point[1]]);
                }
                var thing = new ol.geom.LineString(coordinates);
                var featurething = new ol.Feature({
                    name: "LineString",
                    obj: el,
                    geometry: thing,
                });
                featurething.setId(el._id);
                if (el._id in vm.selectedEl) {
                    featurething.setStyle(modifyPolygonStyle);
                } else {
                    featurething.setStyle(el.style);
                }
                if (el.color) {
                    featurething.setStyle(createLineStyle(el.color, 3));
                }
                vm.source.addFeature(featurething);
                vm.features[el._id] = featurething;
            }

            vm.addPolygonFeature = function (el, style) {
                var coordinates = [];
                for (var i in el.geo.coordinates[0]) {
                    var point = ol.proj.fromLonLat(el.geo.coordinates[0][i]);
                    console.log([point[0], point[1]])
                    coordinates.push([point[0], point[1]]);
                }
                var thing = new ol.geom.Polygon([coordinates]);
                var featurething = new ol.Feature({
                    name: "Polygon",
                    obj: el,
                    geometry: thing,
                });
                featurething.setId(el._id);
                if (el._id in vm.selectedEl) {
                    featurething.setStyle(modifyPolygonStyle);
                } else {
                    if (el.strokeColor, el.strokeWidth, el.fillColor) {
                        featurething.setStyle(createPolygonStyle(el.strokeColor, el.strokeWidth, el.fillColor));
                    } else {
                        featurething.setStyle(el.style);
                    }
                }
                if (vm.modifyFeature && vm.modifyFeature.getId() == featurething.getId()) {
                    vm.source.addFeature(vm.modifyFeature);
                } else {
                    vm.source.addFeature(featurething);
                    vm.features[el._id] = featurething;
                }
            }

            vm.loadSingleLayer = function (layer, box) {
                vm.loading = true;
                vm.limit = 5000;
                var data = {
                    'box': box,
                    'skip': vm.skip,
                    'limit': vm.limit,
                    'layer': layer.name
                }
                vm.box = box;
                var req = {
                    method: "PUT",
                    data: data,
                    // url: "/api/map-light/layers"
                    // url: "http://localhost:8082/api/geo_stations"
                    url: layer.api
                }
                $http(req).then(
                    function (resp) {
                        var style = new ol.style.Style({
                            stroke: new ol.style.Stroke({
                                color: layer.stroke.color,
                                width: layer.stroke.width
                            }),
                            zIndex: 0
                        });
                        var styles = {};
                        styles['default'] = style;

                        if(layer.geometryType == "Point"){
                            style = new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 3,
                                    stroke: new ol.style.Stroke({ color: layer.stroke.color, width: layer.stroke.width })
                                })
                            })
                            
                            styles['default'] =  new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 3,
                                    stroke: new ol.style.Stroke({ color: "#aaa", width: layer.stroke.width })
                                })
                            })

                            styles['urban'] =  new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 3,
                                    stroke: new ol.style.Stroke({ color: "#ff0000", width: layer.stroke.width })
                                })
                            })
                            styles['rural'] =  new ol.style.Style({
                                image: new ol.style.Circle({
                                    radius: 3,
                                    stroke: new ol.style.Stroke({ color: "#009900", width: layer.stroke.width })
                                })
                            })
                        }
                        
                        console.log(layer);
                        for (var i in resp.data.rez) {
                            var el = resp.data.rez[i];
                            el.style = style;
                            if (layer.geometryType == "Point") {
                                vm.addPointFeature(el, styles);
                            } else if (layer.geometryType == "LineString") {
                                vm.addLineStringFeature(el, style);
                            } else if (layer.geometryType == "Polygon") {
                                vm.addPolygonFeature(el, style);
                            }
                        }
                        vm.loading = false;
                        console.log('response processed');
                    }, function (resp) {
                        vm.message = 'error';
                    });

            }

            vm.viewChanged = function (box) {
                for (var key in vm.layers) {
                    var layer = vm.layers[key];
                    if (layer.visible && layer.api) {
                        vm.loadSingleLayer(layer, box);
                    }
                }
            }

            vm.viewChanged_old = function (box) {
                vm.loading = true;
                vm.limit = 5000;
                var data = {
                    'box': box,
                    'skip': vm.skip,
                    'limit': vm.limit
                }
                vm.box = box;
                var visible_layers = [];
                for (var key in vm.layers) {
                    if (vm.layers[key].visible) {
                        visible_layers.push(key);
                    }
                }
                data['visible_layers'] = visible_layers;

                var req = {
                    method: "PUT",
                    data: data,
                    // url: "/api/map-light/layers"
                    url: "http://localhost:8082/api/geo_stations"
                }
                $http(req).then(
                    function (resp) {
                        var count = resp.data.count;
                        var style = new ol.style.Style({
                            stroke: new ol.style.Stroke({
                                color: "#0f0",
                                width: 1
                            }),
                            zIndex: 0
                        });
                        for (var i in resp.data.rez) {
                            var el = resp.data.rez[i];
                            var key = el.layer;
                            var zIndex = 0;
                            if (el.layer in vm.layers) {
                                var key_obj = vm.layers[el.layer];
                                zIndex = key_obj.order;
                                style = new ol.style.Style({
                                    stroke: new ol.style.Stroke({
                                        color: key_obj.stroke.color,
                                        width: key_obj.stroke.width
                                    }),
                                    zIndex: zIndex
                                })
                            }

                            if (key in vm.layers) {
                                vm.layers[key].name = key;
                            } else {
                                var obj = {};
                                obj.name = key;
                                obj.visible = true;
                                obj.stroke = { color: '#f9d5e5', width: 1 }
                                obj.style = new ol.style.Style({
                                    stroke: new ol.style.Stroke({
                                        color: obj.stroke.color,
                                        width: obj.stroke.width
                                    })
                                })
                                obj.style = style;
                                obj.shapes = [];
                                obj.visible = true;
                                vm.layers[key] = obj;
                                vm.store_layer_meta(obj);
                            }
                            if (vm.layers[key].visible == false) {
                                continue;
                            }
                            el.style = style;
                            if (el.geo.type == 'Polygon') {
                                var coordinates = [];
                                for (var i in el.geo.coordinates[0]) {
                                    var point = ol.proj.fromLonLat(el.geo.coordinates[0][i]);
                                    coordinates.push([point[0], point[1]]);
                                }
                                var thing = new ol.geom.Polygon([coordinates]);
                                var featurething = new ol.Feature({
                                    name: "Polygon",
                                    obj: el,
                                    geometry: thing,
                                });
                                featurething.setId(el._id);
                                if (el._id in vm.selectedEl) {
                                    featurething.setStyle(modifyPolygonStyle);
                                } else {
                                    if (el.strokeColor, el.strokeWidth, el.fillColor) {
                                        featurething.setStyle(createPolygonStyle(el.strokeColor, el.strokeWidth, el.fillColor));
                                    } else {
                                        featurething.setStyle(el.style);
                                    }
                                }
                                if (vm.modifyFeature && vm.modifyFeature.getId() == featurething.getId()) {
                                    console.log(vm.modifyFeature.getId())
                                    vm.source.addFeature(vm.modifyFeature);
                                } else {
                                    vm.source.addFeature(featurething);
                                    vm.features[el._id] = featurething;
                                }
                            } else if (el.geo.type == 'Point') {
                                var epoint = ol.proj.fromLonLat(el.geo.coordinates);
                                epoint = [epoint[0], epoint[1]];
                                var thing = new ol.geom.Point(epoint);
                                var featurething = new ol.Feature({
                                    name: "Point",
                                    obj: el,
                                    geometry: thing,
                                });
                                featurething.setId(el._id);
                                if (el.color) {
                                    featurething.setStyle(createPointStyle(el.color, 5, 1));
                                    // }else{
                                    //     featurething.setStyle(createPointStyle("#ff0000", 2, 1));
                                }
                                vm.source.addFeature(featurething);
                                vm.features[el._id] = featurething;

                            } else {
                                var coordinates = [];
                                for (var i in el.geo.coordinates) {
                                    var point = ol.proj.fromLonLat(el.geo.coordinates[i]);
                                    coordinates.push([point[0], point[1]]);
                                }
                                var thing = new ol.geom.LineString(coordinates);
                                var featurething = new ol.Feature({
                                    name: "LineString",
                                    obj: el,
                                    geometry: thing,
                                });
                                featurething.setId(el._id);
                                if (el._id in vm.selectedEl) {
                                    featurething.setStyle(modifyPolygonStyle);
                                } else {
                                    featurething.setStyle(el.style);
                                }
                                if (el.color) {
                                    featurething.setStyle(createLineStyle(el.color, 3));
                                }
                                vm.source.addFeature(featurething);
                                vm.features[el._id] = featurething;
                            }
                        }
                        for (var k in vm.extraFeatures) {
                            var featurething = vm.extraFeatures[k];
                            vm.source.addFeature(featurething);
                        }
                        vm.loading = false;
                        console.log('response processed');
                    }, function (resp) {
                        vm.message = 'error';
                    });
            }
            /******************************************* */
            vm.sketch;
            vm.helpTooltipElement;
            vm.helpTooltip;
            vm.measureTooltipElement;
            vm.measureTooltip;
            vm.continuePolygonMsg = 'Click to continue drawing the polygon';
            vm.continueLineMsg = 'Click to continue drawing the line';

            vm.createHelpTooltip = function () {
                if (vm.helpTooltipElement) {
                    vm.helpTooltipElement.parentNode.removeChild(vm.helpTooltipElement);
                }
                vm.helpTooltipElement = document.createElement('div');
                vm.helpTooltipElement.className = 'tooltip1 hidden';
                vm.helpTooltip = new ol.Overlay({
                    element: vm.helpTooltipElement,
                    offset: [15, 0],
                    positioning: 'center-left'
                });
                vm.map.addOverlay(vm.helpTooltip);
            }

            vm.removeHelpTooltip = function () {
                if (vm.helpTooltipElement) {
                    vm.helpTooltipElement.parentNode.removeChild(vm.helpTooltipElement);
                    vm.helpTooltipElement = null;
                }
            }

            vm.createMeasureTooltip = function () {
                if (vm.measureTooltipElement) {
                    vm.measureTooltipElement.parentNode.removeChild(vm.measureTooltipElement);
                }
                vm.measureTooltipElement = document.createElement('div');
                vm.measureTooltipElement.className = 'tooltip1 tooltip1-measure';
                vm.measureTooltip = new ol.Overlay({
                    element: vm.measureTooltipElement,
                    offset: [0, -15],
                    positioning: 'bottom-center'
                });
                vm.map.addOverlay(vm.measureTooltip);
            }
            /******************************************* */

            vm.map = null;
            vm.draw;

            vm.selectedFeature = null;
            vm.selectedLayer = null;
            vm.selected = [];

            vm.resetInteractions = function () {
                vm.map.removeInteraction(vm.draw);
                vm.removeHelpTooltip();
            }

            vm.measureTool = function () {
                if (vm.tool == 'MeasureLineString') {
                    if (vm.draw) {
                        vm.map.removeInteraction(vm.draw);
                        if (vm.measureTooltipElement) {
                            vm.measureTooltipElement.parentNode.removeChild(vm.measureTooltipElement);
                            vm.measureTooltipElement.className = 'tooltip1 tooltip1-static';
                            vm.measureTooltip.setOffset([0, -7]);
                            vm.measureTooltipElement = null;
                        }
                        vm.removeHelpTooltip();
                    }

                    vm.tool = '';
                } else {
                    vm.addInteractionOn('MeasureLineString', '')
                }
            }

            vm.toggleSelectInteraction = function () {
                if (vm.tool == 'Select') {
                    vm.tool = '';
                    if (vm.draw)
                        vm.map.removeInteraction(vm.draw);
                    if (vm.selectIt)
                        vm.map.removeInteraction(vm.selectIt)
                    vm.draw = null;
                    vm.selectIt = null;
                    vm.selected = [];
                    vm.selectedFeatures.clear();

                    if (vm.selectedEl) {
                        for (var key in vm.selectedEl) {
                            var obj = vm.selectedEl[key];
                            var feature = vm.features[obj._id];
                            if (obj && obj.style)
                                feature.setStyle(obj.style);
                        }
                    }

                    vm.selectedEl = {};

                } else {
                    vm.addInteractionOn('Select', '');
                }
            }

            vm.onFeatureAdded = null;
            vm.addInteractionOn = function addInteraction(value, name) {
                var source = vm.source;//vm.layers[name].source;

                vm.selectedLayer = vm.layers[name];
                vm.tool = 'add';
                if (value == 'Modify')
                    vm.tool = 'Modify';

                if (value == 'Select')
                    vm.tool = 'Select';

                if (value == 'MeasureLineString')
                    vm.tool = 'MeasureLineString';


                if (value !== 'None') {
                    if (vm.draw)
                        vm.map.removeInteraction(vm.draw);
                    var showHelp = false;
                    if (value == "Modify") {
                        if (vm.modifyFeature) {
                            var collection = new ol.Collection();
                            var length = collection.getArray().push(vm.modifyFeature);
                            collection.set('length', length);
                            vm.draw = new ol.interaction.Modify({ features: collection });
                        } else {
                            vm.draw = new ol.interaction.Modify({ source: vm.source });
                        }
                    } else if (value == "Select") {
                        vm.selectIt = new ol.interaction.Select();
                        vm.draw = vm.selectIt;
                        vm.map.addInteraction(vm.selectIt);
                        vm.selectedFeatures = vm.selectIt.getFeatures();
                        var dragBox = new ol.interaction.DragBox({
                            condition: ol.events.condition.shiftKeyOnly,
                            style: new ol.style.Style({
                                stroke: new ol.style.Stroke({
                                    color: [0, 0, 255, 1]
                                })
                            })
                        });
                        vm.draw = dragBox;
                        dragBox.on('boxend', function () {
                            var extent = dragBox.getGeometry().getExtent();
                            vm.selected = [];
                            vm.selectedFeatures.clear();

                            if (vm.selectedEl) {
                                for (var key in vm.selectedEl) {
                                    var obj = vm.selectedEl[key];
                                    var feature = vm.features[obj._id];
                                    if (obj && obj.style)
                                        feature.setStyle(obj.style);
                                }
                            }

                            vm.selectedEl = {};

                            vm.source.forEachFeatureIntersectingExtent(extent, function (feature) {
                                var obj = feature.get('obj')
                                vm.selectedEl[obj._id] = obj;
                                vm.selectedFeatures.push(feature);
                                feature.setStyle(modifyPolygonStyle);
                                vm.map.updateSize();
                            });

                            if (vm.onBoxEnd) {
                                vm.onBoxEnd(vm.selected);
                            }
                        });

                        dragBox.on('boxstart', function () {
                            vm.selectedFeatures.clear();
                            if (vm.selected) {
                                vm.selected = [];
                            }
                        });

                        vm.selectedFeatures.on('add', function (event) {
                            var feature = event.element;
                            if (vm.onObjectSelected) {
                                vm.onObjectSelected(feature);
                            }
                            var obj = feature.get('obj');
                            if (obj) {
                                vm.selectedEl[obj._id] = obj;
                                feature.setStyle(modifyPolygonStyle);
                                //     obj.isSelected = true;
                            }
                            vm.selected.push(feature);
                            $scope.$apply();
                        });

                        vm.map.on('click', function () {
                            vm.selectedFeatures.clear();
                            if (vm.selectedEl) {
                                for (var key in vm.selectedEl) {
                                    var obj = vm.selectedEl[key];
                                    var feature = vm.features[obj._id];
                                    if (feature)
                                        feature.setStyle(obj.style);
                                }
                            }
                        });

                        vm.draw.on('select', function (event) {
                            event.selected.forEach(function (e) {
                                vm.selectedFeature = e.getProperties();
                                var obj = vm.selectedFeature.get('obj')
                                vm.selectedEl[obj._id] = obj;

                                $scope.$apply();
                            });
                        });
                    } else if (value == "Polygon") {
                        vm.draw = new ol.interaction.Draw({ source: source, type: "Polygon" });
                        showHelp = true;
                    } else if (value == "MeasureLineString") {
                        showHelp = true;
                        vm.draw = new ol.interaction.Draw({
                            source: source,
                            type: "LineString"
                        });
                    } else {
                        showHelp = true;
                        var type = vm.selectedLayer.geometryType;
                        if (type == undefined)
                            type = 'LineString';
                        vm.draw = new ol.interaction.Draw({
                            source: source,
                            type: type
                        });
                    }

                    vm.map.addInteraction(vm.draw);
                    var snap = new ol.interaction.Snap({
                        source: vm.source
                    });
                    vm.map.addInteraction(snap);
                    if (showHelp) {
                        vm.createMeasureTooltip();
                        vm.createHelpTooltip();

                        vm.listener;
                        vm.draw.on('drawstart', function (evt) {
                            vm.sketch = evt.feature;
                            var tooltipCoord = evt.coordinate;
                            vm.listener = vm.sketch.getGeometry().on('change', function (evt) {
                                var geom = evt.target;
                                var output;
                                if (geom instanceof ol.geom.Polygon) {
                                    output = formatArea(geom);
                                    tooltipCoord = geom.getInteriorPoint().getCoordinates();
                                } else if (geom instanceof ol.geom.LineString) {
                                    output = formatLength(geom);
                                    tooltipCoord = geom.getLastCoordinate();
                                }
                                vm.measureTooltipElement.innerHTML = output;
                                vm.measureTooltip.setPosition(tooltipCoord);
                            });
                        }, this);

                        vm.draw.on('drawend', function (e) {
                            var geometry = e.feature.getGeometry();
                            // var dx = 0; 
                            // var dy = 0; 
                            // var dx = vm.dx;//-0.1;//0;//-0.1;
                            // var dy = vm.dy;//3;//-3.4;//3;
                            // if (vm.selectedLayer && vm.selectedLayer.dx){
                            //     dx = parseFloat(vm.selectedLayer.dx);
                            //     dy = parseFloat(vm.selectedLayer.dy);
                            // }
                            if (geometry instanceof ol.geom.Polygon) {
                                var length = ol.Sphere.getLength(geometry);
                                var coordinates = geometry.getCoordinates();

                                var geom = geometry.clone().transform('EPSG:3857', 'EPSG:4326');
                                var gcoordinates = geom.getLinearRing(0).getCoordinates();
                                var area = Math.abs(wgs84Sphere.geodesicArea(gcoordinates));
                                var ffeature = {
                                    'layer': vm.selectedLayer.name,
                                    'distance': length,
                                    'size': area,
                                    'geo': { 'type': 'Polygon', 'coordinates': [[]] }
                                };
                                e.feature.set("obj", ffeature);
                                for (var cc in coordinates[0]) {
                                    var coord = coordinates[0][cc];
                                    // coord[0] += dx;
                                    // coord[1] += dy;
                                    coord = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
                                    var lon = coord[0];
                                    var lat = coord[1];
                                    ffeature.geo.coordinates[0].push(coord)
                                }
                                console.log(ffeature);
                                if (vm.selectedLayer.name == 'Projects' || vm.selectedLayer.name == 'Tasks') {
                                    if (vm.onFeatureAdded != null) {
                                        vm.onFeatureAdded(ffeature);
                                        //vm.onFeatureAdded = null;
                                    }
                                } else {
                                    // ----------- DIALOG --------------------
                                    $rootScope.dialog.show(function (obj) { // ---- ok -----
                                        ffeature.id = obj.id.value;
                                        // ffeature.datum = obj.datum.value;
                                    }, function () { // ------ cancel ------
                                        console.log('Cancel');
                                    });

                                    var req = {
                                        method: "PUT",
                                        data: ffeature,
                                        url: "/api/map-light/add_feature_geo"
                                    }
                                    $http(req).then(
                                        function (resp) {
                                            if (vm.onFeatureAdded != null) {
                                                vm.onFeatureAdded(ffeature);
                                                vm.onFeatureAdded = null;
                                            }
                                        },
                                        function (resp) {
                                            console.log('add_feature_geo')
                                        }
                                    );
                                }

                            } else if (geometry instanceof ol.geom.LineString) {
                                if (vm.tool != 'MeasureLineString') {
                                    var length = ol.Sphere.getLength(geometry);
                                    var coordinates = geometry.getCoordinates();
                                    var ffeature = { 'layer': vm.selectedLayer.name, 'distance': length, 'geo': { 'type': 'LineString', 'coordinates': [] } };
                                    for (var cc in coordinates) {
                                        var coord = coordinates[cc];
                                        // coord[0] += dx;
                                        // coord[1] += dy;
                                        coord = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
                                        var lon = coord[0];
                                        var lat = coord[1];
                                        ffeature.geo.coordinates.push(coord)
                                    }
                                    console.log(ffeature);
                                    var req = {
                                        method: "PUT",
                                        data: ffeature,
                                        url: "/api/map-light/add_feature_geo"
                                    }
                                    $http(req).then(
                                        function (resp) {
                                            if (vm.onFeatureAdded != null) {
                                                vm.onFeatureAdded(ffeature);
                                                vm.onFeatureAdded = null;
                                            }
                                        },
                                        function (resp) { }
                                    );
                                }
                            } else if (geometry instanceof ol.geom.Point) {
                                if (vm.tool != 'MeasureLineString') {
                                    var length = ol.Sphere.getLength(geometry);
                                    var coordinates = geometry.getCoordinates();
                                    var ffeature = {
                                        'layer': vm.selectedLayer.name,
                                        'example': 's-shape-',
                                        'distance': length, 'geo': { 'type': 'Point', 'coordinates': ol.proj.transform(coordinates, 'EPSG:3857', 'EPSG:4326') }
                                    };
                                    console.log(ffeature);
                                    var req = {
                                        method: "PUT",
                                        data: ffeature,
                                        url: "/api/map-light/add_feature_geo"
                                    }
                                    $http(req).then(
                                        function (resp) {
                                            if (vm.onFeatureAdded != null) {
                                                vm.onFeatureAdded(ffeature);
                                                vm.onFeatureAdded = null;
                                            }
                                        },
                                        function (resp) { }
                                    );
                                }
                            }//else                            

                            vm.map.removeOverlay(vm.measureTooltip);
                            // vm.measureTooltipElement.className = 'tooltip1 tooltip1-static';
                            // vm.measureTooltip.setOffset([0, -7]);
                            // unset sketch
                            vm.sketch = null;
                            vm.measureTooltipElement = null;
                            vm.createMeasureTooltip();
                            ol.Observable.unByKey(vm.listener);
                        }, this);

                    } else {
                        vm.removeHelpTooltip()
                    }
                    vm.draw.on('modifyend', function (e) {
                        var features = e.features.getArray();
                        for (var i = 0; i < features.length; i++) {
                            var geometry = features[i].getGeometry();
                            var obj = features[i].get('obj');
                            // if (vm.modifyFeature && vm.modifyFeature != features[i])
                            // if (vm.selectedFeature && vm.selectedFeature != features[i])
                            //     continue;
                            //if (vm.selected && vm.selected[0] != features[i])
                            //    continue;
                            if (vm.selectedEl && vm.selectedEl[obj._id]) {
                                if (geometry instanceof ol.geom.Polygon) {
                                    var geom = geometry.clone().transform('EPSG:3857', 'EPSG:4326');
                                    var ccoordinates = geom.getLinearRing(0).getCoordinates();
                                    var area = Math.abs(wgs84Sphere.geodesicArea(ccoordinates));

                                    var coordinates = geometry.getCoordinates();
                                    var ffeature = {
                                        '_id': features[i].getId(),
                                        'size': area,
                                        'distance': area,
                                        'area': area,
                                        'layer': obj.layer,
                                        'geo': { 'type': 'Polygon', 'coordinates': [[]] }
                                    };
                                    for (var cc in coordinates[0]) {
                                        var coord = coordinates[0][cc];
                                        // coord[0] += dx;
                                        // coord[1] += dy;
                                        coord = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
                                        var lon = coord[0];
                                        var lat = coord[1];
                                        ffeature.geo.coordinates[0].push(coord)
                                    }
                                    features[i].set("obj", ffeature);
                                    // 'Road_markings_pc', 'Curbs', 'Ts_poles'
                                    if (obj.layer == 'Projects' || obj.layer == 'Tasks') {
                                        if (vm.onFeatureModified) {
                                            vm.onFeatureModified(ffeature);
                                        }
                                    } else {
                                        var req = {
                                            method: "PUT",
                                            data: ffeature,
                                            url: "/api/map-light/update_feature_geo"
                                        }
                                        $http(req).then(
                                            function (resp) { },
                                            function (resp) { }
                                        );
                                    }
                                } else {
                                    var coordinates = geometry.getCoordinates();
                                    var ffeature = { '_id': features[i].getId(), 'geo': { 'type': 'LineString', 'coordinates': [] } };
                                    for (var cc in coordinates) {
                                        var coord = coordinates[cc];
                                        // coord[0] += vm.dx;
                                        // coord[1] += vm.dy;
                                        coord = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
                                        var lon = coord[0];
                                        var lat = coord[1];
                                        ffeature.geo.coordinates.push(coord)
                                    }
                                    // console.log(ffeature);
                                    // if (obj.layer == 'A-B') {
                                    //     if (vm.onFeatureModified) {
                                    //         vm.onFeatureModified(ffeature);
                                    //     }
                                    // } else {
                                    var req = {
                                        method: "PUT",
                                        data: ffeature,
                                        url: "/api/map-light/update_feature_geo"
                                    }
                                    $http(req).then(
                                        function (resp) { },
                                        function (resp) { }
                                    );
                                    // }
                                }//else



                            }
                        }
                    });
                }
            }

            vm.setVisible = function (layer_name, value) {
                vm.layers[layer_name].visible = value;
                vm.store_layer_meta(vm.layers[layer_name]);
                console.log('set visible', layer_name);
                vm.refreshView();
            }

            vm.selected = [];

            vm.refreshStyles = function () {
                for (var key in vm.layers) {
                    var el = vm.layers[key];
                    var sstyle = new ol.style.Style({});
                    if (el.stroke.width != 0) {
                        sstyle = new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: 10,
                                fill: new ol.style.Fill({ color: '#666666' }),
                                stroke: new ol.style.Stroke({ color: '#bada55', width: 1 })
                            })
                        })
                    }

                    vm.layers[key].style = sstyle
                }
                console.log('refresh styles');
            }

            vm.removeSelected = function () {
                console.log(vm.selected);
                $rootScope.dialog.showYesNo('Are you sure?',
                    function (obj) {
                        vm.selectedCount = vm.selected.length;
                        for (var i in vm.selected) {
                            var el = vm.selected[i];
                            vm.source.removeFeature(el);
                            var req = {
                                method: "PUT",
                                data: el.get('obj'),
                                url: "/api/map-light/remove_feature_geo"
                            }
                            $http(req).then(
                                function (resp) {
                                    vm.selectedCount--;
                                    if (vm.selectedCount == 0) {
                                        vm.selectedFeatures.clear();//remove(el);
                                        vm.selectedEl = {};
                                        vm.selected = [];
                                    }
                                },
                                function (resp) { }
                            );
                        }
                    })
            }

            vm.initLayers = function () {
                for (var key in vm.layers) {
                    var el = vm.layers[key];
                    el.name = key;
                    el.style = new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: el.stroke.color,
                            width: el.stroke.width
                        }),
                        image:
                            new ol.style.Circle({
                                fill: new ol.style.Fill({
                                    color: 'red'
                                }),
                                radius2: 3
                            }),
                    })
                    if (key == 'table') {
                        el.style = function polygonStyleFunction(feature, resolution) {
                            return new ol.style.Style({
                                stroke: new ol.style.Stroke({
                                    color: el.stroke.color,
                                    width: el.stroke.width
                                }),
                                fill: new ol.style.Fill({
                                    color: 'rgba(0, 255, 255, 0.1)'
                                }),
                                text: createTextStyle(feature, resolution)
                            });
                        }
                    }

                    el.source = new ol.source.Vector();
                    el.layer = new ol.layer.Vector({
                        source: el.source,
                        name: key,
                    });
                    // el.shapes = [];
                    vm.map.addLayer(el.layer);

                    vm.source = new ol.source.Vector();
                    vm.layer = new ol.layer.Vector({
                        source: vm.source,
                        name: 'SVE',
                        rendererOptions: { zIndexing: true }
                    });
                    vm.map.addLayer(vm.layer);

                    //--------------- ROVERI -------------------------
                    vm.rSource = new ol.source.Vector();
                    vm.rLayer = new ol.layer.Vector({
                        source: vm.rSource,
                        name: 'roveri',
                        style: roverStyle
                    });
                    vm.map.addLayer(vm.rLayer);

                    // //--------------- CAAMERA -------------------------
                    // vm.cSource = new ol.source.Vector();
                    // vm.cLayer = new ol.layer.Vector({
                    //     source: vm.cSource,
                    //     name: 'camera',
                    //     style: roverStyle
                    // });
                    // vm.map.addLayer(vm.cLayer);
                }
            }

            vm.init = function () {
                /****************************** */
                var proj4Def3857 = "+proj=merc +lat_0=0 +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs "; // From http://spatialreference.org/ref/sr-org/6864/
                proj4.defs("proj3857D", proj4Def3857);
                ol.proj.addCoordinateTransforms('EPSG:3857', "proj3857D",
                    function (coordinate) {
                        return [coordinate[0], coordinate[1]];
                    },
                    function (coordinate) {
                        return [coordinate[0], coordinate[1]];
                    });

                var projection3857 = ol.proj.get('EPSG:3857'); // The default
                var extent3857 = projection3857.getExtent();
                // // For the shifted projection
                var projection = ol.proj.get("proj3857D");
                projection.setExtent(extent3857); // extent is needed to enable reprojection
                /****************************** */
                vm.gmap = new ol.layer.Tile({
                    //   name: 'gmap',
                    visible: true,
                    title: "Google Satellite",
                    source: new ol.source.TileImage({
                        projection: 'proj3857D',
                        url: 'http://mt1.google.com/vt/lyrs=s&hl=pl&&x={x}&y={y}&z={z}'
                    }),
                });

                vm.osm_mapa = new ol.layer.Tile({
                    visible: false,
                    source: new ol.source.OSM()
                })

                vm.bing_mapa1 = new ol.layer.Tile({
                    visible: false,
                    source: new ol.source.BingMaps({
                        key: 'Aq9R6UW215ihWE5oxEc2GpBckZFYnaV5bkpETNrX5gkww51KeYhfo4GVK2Zz7bdT',
                        imagerySet: 'Aerial'
                    })
                })

                vm.bing_mapa2 = new ol.layer.Tile({
                    visible: false,
                    source: new ol.source.BingMaps({
                        key: 'Aq9R6UW215ihWE5oxEc2GpBckZFYnaV5bkpETNrX5gkww51KeYhfo4GVK2Zz7bdT',
                        imagerySet: 'Road'
                    })
                })

                vm.map_center = [139.90738034248352, 36.488851033708];

                var map_center = $window.localStorage.getItem('map-map_center');
                var zoom = $window.localStorage.getItem('map-zoom');
                if (zoom == undefined)
                    zoom = 16;

                if (map_center != undefined) {
                    vm.map_center = JSON.parse(map_center);
                }

                vm.map = new ol.Map({
                    logo: false,
                    controls: ol.control.defaults().extend([
                    ]),
                    layers: [
                        vm.osm_mapa, vm.gmap, vm.bing_mapa1, vm.bing_mapa2
                    ],
                    target: 'map',
                    // projection: vm.projection,
                    view: new ol.View({
                        //projection: 'EPSG:27700',
                        center: ol.proj.fromLonLat(vm.map_center),
                        zoom: zoom,
                        // rotation: Math.PI / 6,
                        maxZoom: 40
                    })
                });
                vm.map_tiles_index = 0;
                vm.map_tiles = [vm.osm_mapa, vm.gmap, vm.bing_mapa1, vm.bing_mapa2];

                vm.nextMap = function () {
                    vm.map_tiles[vm.map_tiles_index].setVisible(false);
                    vm.map_tiles_index += 1;
                    if (vm.map_tiles_index == vm.map_tiles.length) {
                        vm.map_tiles_index = 0;
                    }
                    vm.map_tiles[vm.map_tiles_index].setVisible(true);
                }

                function pointerMoveHandler(evt) {
                    if (evt.dragging) {
                        return;
                    }
                    var helpMsg = 'Click to start drawing';

                    if (vm.sketch) {
                        var geom = (vm.sketch.getGeometry());
                        if (geom instanceof ol.geom.Polygon) {
                            helpMsg = vm.continuePolygonMsg;
                        } else if (geom instanceof ol.geom.LineString) {
                            helpMsg = vm.continueLineMsg;
                        }
                    }

                    if (vm.helpTooltipElement) {
                        vm.helpTooltipElement.innerHTML = helpMsg;
                        vm.helpTooltip.setPosition(evt.coordinate);
                        vm.helpTooltipElement.classList.remove('hidden');
                    } else {
                        var mouseCoordInMapPixels = [evt.originalEvent.offsetX, evt.originalEvent.offsetY];
                        var coord = evt.coordinate;
                        coord = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
                        var el = jq('#atCursor');
                        if (el) {
                            vm.map.getViewport().style.cursor = '';
                            vm.map.forEachFeatureAtPixel(mouseCoordInMapPixels, function (feature, layer) {
                                var obj = feature.get('obj');
                                if (obj) {
                                    vm.map.getViewport().style.cursor = 'pointer';
                                }
                                return true;
                            });
                        }
                    }
                };

                vm.singleClickHandler = null;
                var singleClick = function (evt) {
                    var el = jq('#atCursor');
                    if (el) {
                        vm.map.on('singleclick', function (evt) {
                            var feature = vm.map.forEachFeatureAtPixel(evt.pixel,
                                function (feature, layer) {
                                    var obj = feature.get('obj');
                                    if (obj) {
                                        if (vm.singleClickHandler)
                                            vm.singleClickHandler(obj);
                                        if (obj.rov_id) {
                                            el[0].innerHTML = '[' + obj.rov_id + ' ' + obj.date + ']';
                                        }
                                        if (obj.file_name) {
                                            el[0].innerHTML = obj.file_name;
                                        }
                                        if (obj.properties) {
                                            if (obj.properties.image) {
                                                el[0].innerHTML = obj.properties.image;
                                                vm.ws.send('image:' + obj.properties.image);

                                                // proveriti koliki je head i nacrtati mrezu ispred vozila
                                                cameraViewLines(obj, vm.source, vm.features);
                                            }
                                        }
                                    }
                                    return [feature, layer];
                                });
                        });
                    }
                    var el1 = jq('#pc_viewer_a');
                    if (el1) {
                        var commandLine = jq('#commandLine');
                        var coordinate = evt.coordinate;
                        var wgs_point = ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326');
                        commandLine[0].value = wgs_point;
                        var selected = null;
                        var feature = vm.map.forEachFeatureAtPixel(evt.pixel,
                            function (feature, layer) {
                                var obj = feature.get('obj');
                                if (obj && obj.layer != 'Sector' && obj.layer != 'Projects') {
                                    if (obj.file_name && selected == null) {
                                        el[0].innerHTML = obj.file_name;
                                        el1[0].href = "pc_viewer.html?fileName=" + obj.file_name;
                                        vm.selectedEl = {};
                                        vm.selectedEl[obj._id] = obj;
                                        vm.refreshView();
                                    } else if (obj.properties && obj.properties.image) {
                                        var centar = obj.geo.coordinates[0];
                                        var z = obj.properties.z;
                                        var heading = obj.properties.heading;
                                        var roll = obj.properties.roll;
                                        var pitch = obj.properties.pitch;
                                        el1[0].href = "panorama.html?fileName=" + obj.properties.image;
                                        vm.selectedEl = {};
                                        vm.selectedEl[obj._id] = obj;
                                        vm.refreshView();
                                    }
                                    selected = obj;
                                    console.log(obj);
                                    return;
                                }
                            });
                    }
                };

                var moveEnd = function () {
                    var rr = vm.map.getView().calculateExtent(vm.map.getSize());
                    var box = ol.proj.transformExtent(rr, 'EPSG:3857', 'EPSG:4326');
                    var bbox = [];
                    bbox.push([box[0], box[1]])
                    bbox.push([box[2], box[1]])
                    bbox.push([box[2], box[3]])
                    bbox.push([box[0], box[3]])
                    bbox.push([box[0], box[1]])
                    var center = [0, 0];
                    center[0] = (box[0] + box[2]) / 2
                    center[1] = (box[1] + box[3]) / 2
                    $window.localStorage.setItem('map-map_center', JSON.stringify(center));
                    var zoom = vm.map.getView().getZoom();
                    $window.localStorage.setItem('map-zoom', JSON.stringify(zoom));
                    if (vm.viewChanged) {
                        //vm.source.clear()
                        //vm.features = {};
                        vm.limit = 1000;
                        vm.skip = 0;
                        vm.viewChanged([bbox]);
                    }
                };

                vm.map.on('pointermove', pointerMoveHandler);
                vm.map.on('singleclick', singleClick);
                vm.map.on("moveend", moveEnd);

                vm.map.getViewport().addEventListener('mouseout', function () {
                    if (vm.helpTooltipElement)
                        vm.helpTooltipElement.classList.add('hidden');
                });

                vm.initLayers();
            }

            vm.addLayer = function () {
                var el = {};
                el.name = "Layer";
                el.visible = true;
                el.color = "#00ff00";
                el.geometryType = 'LineString';
                el.stroke = {
                    "color": "#0480ed",
                    "width": 1
                };
                el.style = new ol.style.Style({
                    stroke: new ol.style.Stroke(el.stroke)
                })
                el.shapes = [];
                $rootScope.dialog.fields = [
                    { name: "_id", label: '_id', value: '', type: 'text' },
                    { name: "name", label: 'name', value: '', type: 'text' },
                    { name: "color", label: 'color', value: '', type: 'text' },
                    { name: "api", label: 'api', value: '', type: 'text' }
                ];

                $rootScope.dialog.fields[0].value = el._id;
                $rootScope.dialog.fields[1].value = el.name;
                $rootScope.dialog.fields[2].value = el.stroke.color;
                var api = '';
                if (el.api)
                    api = el.api;
                $rootScope.dialog.fields[3].value = api;
                $rootScope.dialog.show(function (obj) { // ---- ok -----
                    el.stroke.color = obj.color.value;
                    el.api = obj.api.value;
                    for (var l in vm.layers) {
                        if (l == el.name) {
                            delete vm.layers[l];
                            el.name = obj.name.value;
                            vm.layers[el.name] = el;
                            break;
                        }
                    }
                    vm.store_layer_meta(el);
                }, function () { // ------ cancel ------
                    console.log('Cancel');
                });

            }

            vm.editLayerMeta = function (el) {
                $rootScope.dialog.fields = [
                    { name: "_id", label: '_id', value: '', type: 'text' },
                    { name: "name", label: 'name', value: '', type: 'text' },
                    { name: "color", label: 'color', value: '', type: 'text' },
                    { name: "api", label: 'api', value: '', type: 'text' }
                ];

                $rootScope.dialog.fields[0].value = el._id;
                $rootScope.dialog.fields[1].value = el.name;
                $rootScope.dialog.fields[2].value = el.stroke.color;
                var api = '';
                if (el.api)
                    api = el.api;
                $rootScope.dialog.fields[3].value = api;
                $rootScope.dialog.show(function (obj) { // ---- ok -----
                    el.stroke.color = obj.color.value;
                    el.api = obj.api.value;
                    for (var l in vm.layers) {
                        if (l == el.name) {
                            delete vm.layers[l];
                            el.name = obj.name.value;
                            vm.layers[el.name] = el;
                            break;
                        }
                    }
                    vm.store_layer_meta(el);
                }, function () { // ------ cancel ------
                    console.log('Cancel');
                });
            }

            jq.getScript("/app/components/map/util.js", function () {
                vm.beforeInit();
            });

            vm.beforeInit = function () {
                var req = {
                    method: "GET",
                    url: "/api/map-light/layers_meta_all"
                };
                $http(req).then(
                    function (resp) {
                        vm.layers = {};
                        for (var i in resp.data.rez) {
                            var el = resp.data.rez[i];
                            vm.layers[el.name] = el;
                        }
                        vm.init();
                    },
                    function (resp) {
                        console.log('greska')
                    }
                );
            }

            $scope.open = function () {
                $scope.showModal = true;
            };

            $scope.ok = function () {
                $scope.showModal = false;
            };

            $scope.cancel = function () {
                $scope.showModal = false;
            };

        }// controller
    ); // app

})();