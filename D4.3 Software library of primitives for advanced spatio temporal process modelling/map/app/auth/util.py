#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask_restful import abort
from functools import wraps
from flask import request
import datetime

import random
import string
import hashlib
# import os

from app.db import mongo_db


class KeyValueCollection():

    def __init__(self, group):
        if group == 'AppUsers':
            self.collection = mongo_db.AppUsers
        elif group == 'AppTokenUsers':
            self.collection = mongo_db.AppTokenUsers
        elif group == 'AppAdminUsers':
            self.collection = mongo_db.AppAdminUsers
        elif group == 'AppAdminTokenUsers':
            self.collection = mongo_db.AppAdminTokenUsers

    def __contains__(self, key):
        ret = self.collection.find_one({'key': key})
        if ret is not None:
            return True
        else:
            return False

    def __getitem__(self, key):
        ret = self.collection.find_one({'key': key})
        if ret is not None:
            return ret['value']
        else:
            return None

    def __setitem__(self, key, value):
        self.collection.update(
            {'key': key},
            {
              'key': key,
              'value': value,
              'logged': datetime.datetime.now()}, upsert=True)

    def get(self, key, default):
        ret = self.__getitem__(key)
        if ret is not None:
            return ret
        else:
            return default

    def set(self, key, value):
        return self.__setitem__(self, key, value)


USERS = KeyValueCollection('AppUsers')
TOKEN_USERS = KeyValueCollection('AppTokenUsers')

ADMIN_USERS = KeyValueCollection('AppAdminUsers')
ADMIN_TOKEN_USERS = KeyValueCollection('AppAdminTokenUsers')


def token_generator():
    n = 32
    alphabet = string.ascii_lowercase
    alphabet += string.ascii_uppercase
    alphabet += string.digits
    token = ''.join(random.choice(alphabet) for _ in range(n))
    return token


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.user is None:
            abort(404, message="Login required")
            return 404
        return f(*args, **kwargs)
    return decorated_function


def admin_login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.admin_user is None:
            abort(404, message="Login required")
            return 404
        return f(*args, **kwargs)
    return decorated_function


def set_password(raw_password):
    algo = 'md5'
    salt = token_generator()
    m = hashlib.md5()
    m.update('%s%s' % (salt, raw_password))
    hsh = m.hexdigest()
    return '%s$%s$%s' % (algo, salt, hsh)


def check_password(raw_password, enc_password):
    """
    Returns a boolean of whether the raw_password was correct. Handles
    encryption formats behind the scenes.
    """
    algo, salt, hshA = enc_password.split('$')
    m = hashlib.md5()
    m.update(salt + raw_password)
    hshB = m.hexdigest()
    return hshA == hshB

if __name__ == "__main__":
    password = "password"

    ep1 = set_password("levo.desno")
    print(ep1) 

    ep2 = check_password("levo.desno1", ep1)
    print(ep2)
